# git2cl

Convert `git` logs to GNU ChangeLog format.

# License

The git2cl tool is released under the GPLv3+.  See the file
[COPYING](COPYING) for detailed information.

# Usage

The tool invokes 'git log' internally unless you pipe a log to it.
Thus, typically you would use it as follows:

```
jas@mocca:~/src/libtasn1$ git2cl > ChangeLog
jas@mocca:~/src/libtasn1$
```

If you don't want git2cl to invoke git log internally, you can use it
as a pipe.  It needs a git log generated with --pretty --numstat and
--summary.  You can use it as follows:

```
jas@mocca:~/src/libtasn1$ git log --pretty --numstat --summary | git2cl > ChangeLog
jas@mocca:~/src/libtasn1$
```

The output format is specified by the [GNU ChangeLog
format](http://www.gnu.org/prep/standards/html_node/Change-Logs.html).

My inspiration for writing this tool was the
[cvs2cl](http://www.red-bean.com/cvs2cl/) tool, which I have been
using in several projects.  Replacing it was necessary to seriously
consider switching from CVS to GIT for my projects.

There is a [Savannah git2cl
project](https://savannah.nongnu.org/projects/git2cl/) and a [GitLab
git2cl project](https://gitlab.com/jas/git2cl). An old repository is
still at [http://repo.or.cz/w/git2cl.git].

# Related work

The [gnulib](https://www.gnu.org/software/gnulib/) project offers a
tool called [gitlog-to-changelog](https://www.gnu.org/software/gnulib/manual/html_node/gitlog_002dto_002dchangelog.html) which is similar.

The `git` tool itself has evolved since `git2cl` was created, and one
command to achieve somewhat different output would be:

```
git log --pretty="format:%ad  %aN  <%aE>%n%n%x09* %s%n" --date=short
```

# Credits

Luis Mondesi contributed several improvements.

# Support

Try talking to [simon@josefsson.org](Simon Josefsson).
